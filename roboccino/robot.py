from supplementary_elements.path_conversion import convert_path_points
from supplementary_elements.robot_enum import RobotState
from supplementary_elements.error_enum import ErrorHandling

from rclpy.exceptions import ParameterNotDeclaredException
from rcl_interfaces.msg import ParameterType
from std_msgs.msg import String, Int8
from rclpy.node import Node
import rclpy

import rtde_control
import signal
import sys

ASYNC = False


class Robot(Node):

    def __init__(self):
        super().__init__('robot')

        self.declare_parameter('robot_ip', '192.168.1.20')
        self.declare_parameter('velocity', 0.3)
        self.declare_parameter('acceleration', 0.3)
        self.declare_parameter('initial_cup_position', [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.declare_parameter('under_powder_dispenser', [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.declare_parameter('under_water_dispenser', [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.declare_parameter('final_position', [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.declare_parameter('mixing_depth', 0.0)
        self.declare_parameter('clump_mixing_depth', 0.0)
        
        
        self.subscription_command = self.create_subscription(Int8, 
                            'robot_command', self.command_callback, 10)
        self.publisher_status = self.create_publisher(Int8, 'robot_status', 10)
       
        self.subscription_error = self.create_subscription(Int8, 'error_signal',
                                                self.error_callback, 10)
        self.publisher_error = self.create_publisher(Int8, 'error_signal', 10)

        self.ip = self.get_parameter('robot_ip').get_parameter_value().string_value
        self.a  = self.get_parameter('velocity').get_parameter_value().double_value
        self.v  = self.get_parameter('acceleration').get_parameter_value().double_value
        
        self.path = {}

        self.robot = rtde_control.RTDEControlInterface(self.ip)
        signal.signal(signal.SIGINT, self.signal_handler)

        self.get_path()


    def command_callback(self, msg):
        cmd = RobotState(msg.data)
        
        if(cmd == RobotState.INITIALIZE):
            movesL = []
            movesJ = []
        
        elif(cmd == RobotState.INITIAL_POSE):
            movesL = ['A_1']
            movesJ = []
        
        elif(cmd == RobotState.GRAB_CUP_FROM_INITIAL):
            movesL = ['A_2', 'B_2', 'C_2']
            movesJ = []

        elif(cmd == RobotState.MOVE_TO_POWDER_DISPENSER):    
            movesL = ['A_3', 'B_3']
            movesJ = ['A_3']
            
        elif(cmd == RobotState.MOVE_TO_WATER_DISPENSER):    
            movesL = ['A_4', 'B_4', 'C_4', 'D_4', 'E_4', 'F_4']
            movesJ = ['B_4']

        elif(cmd == RobotState.POSE_FOR_MIXING):    
            movesL = ['A_5', 'B_5', 'C_5', 'D_5']
            movesJ = []
        
        elif(cmd == RobotState.MOVE_ABOVE_THE_CUP):    
            movesL = ['A_6']
            movesJ = []

        elif(cmd == RobotState.MOVE_TO_PHOTO):    
            movesL = ['A_7', 'B_7']
            movesJ = []
        
        elif(cmd == RobotState.FROM_PHOTO_TO_WAITING):    
            movesL = ['A_8', 'B_8']
            movesJ = []
        
        elif(cmd == RobotState.FROM_WAITING_TO_MIXING):    
            movesL = ['A_9']
            movesJ = []
        
        elif(cmd == RobotState.FROM_WAITING_TO_FOAM_CLUMPS):    
            movesL = ['A_10']
            movesJ = []
        
        elif(cmd == RobotState.GRAB_CUP_FROM_MIXING):    
            movesL = ['A_11', 'B_11', 'C_11', 'D_11', 'E_11', 'F_11','G_11']
            movesJ = []
        
        elif(cmd == RobotState.FINAL_POSE):    
            movesL = ['A_12', 'B_12', 'C_12', 'D_12']
            movesJ = []
        
        else:
            return
            
        self.move_robot(movesL, movesJ)
        if(self.robot.isProgramRunning() != False):
            self.update_status(cmd)


    def move_robot(self, list_of_points, moveJ_list):
        for point in list_of_points:
            if(point in moveJ_list):
                success = self.robot.moveJ_IK(self.path[point], self.v, self.a, ASYNC)
            else:
                success = self.robot.moveL(self.path[point], self.v, self.a, ASYNC)
            if(success == False):
                self.send_error_signal(ErrorHandling.ROBOT_ERROR)
                self.kill_node()
                return


    def update_status(self, msg):
        feedback = Int8()
        feedback.data = msg.value
        self.publisher_status.publish(feedback)
        self.get_logger().info('%s' % msg)


    def get_path(self):
        B_2 = self.get_point('initial_cup_position')
        B_3 = self.get_point('under_powder_dispenser')
        D_4 = self.get_point('under_water_dispenser')
        C_11 = self.get_point('final_position')
        mixing_depth = self.get_parameter('mixing_depth').get_parameter_value().double_value
        clump_mixing_depth = self.get_parameter('clump_mixing_depth').get_parameter_value().double_value
        self.path = convert_path_points(B_2, B_3, D_4, C_11, mixing_depth, clump_mixing_depth)


    def get_point(self, param_name):
        return self.get_parameter(param_name).get_parameter_value().double_array_value.tolist()
    

    def send_error_signal(self, msg):
        to_send = Int8()
        to_send.data = msg.value
        self.publisher_error.publish(to_send)
        self.get_logger().info('%s' % msg)
        self.kill_node()


    def error_callback(self, msg):
        self.kill_node()


    def signal_handler(self, signal, frame):
        self.kill_node()


    def kill_node(self):    
        if(self.robot is not None and self.robot.isConnected()):
            self.robot.stopScript()
            self.robot.disconnect()
        self.destroy_node()
        sys.exit(0)



def main(args=None):
    rclpy.init(args=args)

    robot = Robot()
    rclpy.spin(robot)

    robot.kill_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
