from supplementary_elements.ramp_enum import RampState
from supplementary_elements.error_enum import ErrorHandling

from rclpy.exceptions import ParameterNotDeclaredException
from rcl_interfaces.msg import ParameterType
from std_msgs.msg import UInt8, Int8
from rclpy.node import Node
import rclpy

import signal
import sys


class Ramp(Node):

    def __init__(self):
        super().__init__('ramp')
        
        self.declare_parameter('desired_height', 90)
        self.declare_parameter('max_height', 0)
        self.declare_parameter('min_height', 180)

        self.subscription_command = self.create_subscription(Int8, 
                            'ramp_command', self.command_callback, 10)
        self.publisher_status = self.create_publisher(Int8, 'ramp_status', 10)
        self.publisher_serial_command = self.create_publisher(UInt8, 
                            'ramp_servo_command', 10)
        self.subscription_error = self.create_subscription(Int8, 'error_signal',
                                                self.error_callback, 10)
        self.publisher_error = self.create_publisher(Int8, 'error_signal', 10)

        self.desired_height = 0
        self.max_height = 0
        signal.signal(signal.SIGINT, self.signal_handler) 


    def init_ramp(self):
        self.desired_height = self.get_parameter('desired_height').get_parameter_value().integer_value
        self.max_height = self.get_parameter('max_height').get_parameter_value().integer_value
        self.min_height = self.get_parameter('min_height').get_parameter_value().integer_value


    def command_callback(self, msg):
        cmd = RampState(msg.data)
        if(cmd == RampState.INITIALIZE):
            self.init_ramp()
            self.update_status(cmd)
        elif(cmd == RampState.SET_DESIRED):
            self.send_servo_command(self.desired_height)
        elif(cmd == RampState.SET_MAX):
            self.send_servo_command(self.max_height)
        elif(cmd == RampState.SET_MIN):
            self.send_servo_command(self.min_height)


    def update_status(self, msg):
        feedback = Int8()
        feedback.data = msg.value
        self.publisher_status.publish(feedback)
        self.get_logger().info('%s' % msg)
    

    def send_servo_command(self, h):
        cmd = UInt8()
        cmd.data = h
        self.publisher_serial_command.publish(cmd)
    
    def send_error_signal(self, msg):
        to_send = Int8()
        to_send.data = msg.value
        self.publisher_error.publish(to_send)
        self.get_logger().info('%s' % msg)
        self.kill_node()


    def error_callback(self, msg):
        self.kill_node()


    def signal_handler(self, signal, frame): 
        self.kill_node()


    def kill_node(self):
        self.destroy_node()
        sys.exit(0)



def main(args=None):
    rclpy.init(args=args)

    ramp = Ramp()
    rclpy.spin(ramp)
    
    ramp.kill_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
