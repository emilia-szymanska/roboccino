from supplementary_elements.dispenser_enum import DispenserState
from supplementary_elements.ramp_enum import RampState
from supplementary_elements.serial_enum import SerialState
from supplementary_elements.error_enum import ErrorHandling
from roboccino_interfaces.msg import WaterDispenserParams

from rclpy.exceptions import ParameterNotDeclaredException
from rcl_interfaces.msg import ParameterType
from std_msgs.msg import UInt8, Int8
from rclpy.node import Node
import rclpy

import signal
import serial
import struct
import sys

WATER_DISPENSER_BYTE = 0
RAMP_BYTE = 1

class ServoCommander(Node):

    def __init__(self):
        super().__init__('servo_commander')
        
        self.declare_parameter('device', '/dev/ttyACM1')
        self.declare_parameter('baud_rate', 9600)
        
        self.subscription_serial_command = self.create_subscription(Int8, 
                            'serial_servo_command', self.serial_command_callback, 10)
        self.publisher_serial_status = self.create_publisher(Int8, 'serial_servo_status', 10)

        self.subscription_ramp_command = self.create_subscription(UInt8, 
                            'ramp_servo_command', self.ramp_servo_command_callback, 10)
        self.publisher_ramp_status = self.create_publisher(Int8, 'ramp_status', 10)
        
        self.subscription_dispenser_command = self.create_subscription(WaterDispenserParams, 
                            'dispenser_servo_command', self.dispenser_servo_command_callback, 10)
        self.publisher_dispenser_status = self.create_publisher(Int8, 'water_dispenser_status', 10)
        
        self.subscription_error = self.create_subscription(Int8, 'error_signal',
                                                self.error_callback, 10)
        self.publisher_error = self.create_publisher(Int8, 'error_signal', 10)
        
        self.serial = None
        self.off = 0
        signal.signal(signal.SIGINT, self.signal_handler) 


    def init_serial(self):
        dev = self.get_parameter('device').get_parameter_value().string_value
        br = self.get_parameter('baud_rate').get_parameter_value().integer_value
        self.get_logger().info('Serial device: "%s"' % dev)
        self.get_logger().info('Baud rate: "%d"' % br)
        self.serial = serial.Serial(dev, br)


    def serial_command_callback(self, msg):
        cmd = SerialState(msg.data)
        if(cmd == SerialState.INITIALIZE):
            self.init_serial()
            self.update_serial_status(cmd)
        elif(cmd == SerialState.CLOSE):
            self.serial.close()
            self.update_serial_status(cmd)


    def ramp_servo_command_callback(self, msg):
        h = msg.data
        print('Sending to the ramp')
        self.serial.write(struct.pack('>BBBB', RAMP_BYTE, h, 0, 0))
        h_response = self.get_response()
        
        print('Received response')
        if(h == h_response):
            self.update_ramp_status(RampState.SET)


    def dispenser_servo_command_callback(self, msg):
        on = msg.on_value
        self.off = msg.off_value
        time = msg.time
        
        self.serial.write(struct.pack('>BBBB', WATER_DISPENSER_BYTE, on, self.off, time))
        on_rx = self.get_response()
        off_rx = self.get_response()
        time_rx = self.get_response()
        if(time_rx == time and on_rx == on and off_rx == self.off):
            self.update_water_dispenser_status(DispenserState.POUR)


    def get_response(self):
        feedback = self.serial.readline()
        response = int(feedback.decode())
        return response


    def send_error_signal(self, msg):
        to_send = Int8()
        to_send.data = msg.value
        self.publisher_error.publish(to_send)
        self.get_logger().info('%s' % msg)
        self.kill_node()


    def update_water_dispenser_status(self, msg):
        feedback = Int8()
        feedback.data = msg.value
        self.publisher_dispenser_status.publish(feedback)
        self.get_logger().info('%s' % msg)

    
    def update_ramp_status(self, msg):
        feedback = Int8()
        feedback.data = msg.value
        self.publisher_ramp_status.publish(feedback)
        self.get_logger().info('%s' % msg)

    
    def update_serial_status(self, msg):
        feedback = Int8()
        feedback.data = msg.value
        self.publisher_serial_status.publish(feedback)
        self.get_logger().info('%s' % msg)


    def error_callback(self, msg):
        self.kill_node()

    def kill_node(self):
        if(self.serial is not None):
            self.serial.write(struct.pack('>BBBB', WATER_DISPENSER_BYTE, 0, self.off, self.off))
            self.serial.close()
        self.destroy_node()
        sys.exit(0)

    def signal_handler(self, signal, frame): 
        self.kill_node()


def main(args=None):
    rclpy.init(args=args)

    servo_commander = ServoCommander()
    rclpy.spin(servo_commander)
    
    servo_commander.kill_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
