from supplementary_elements.error_enum import ErrorHandling
from supplementary_elements.decision_enum import Decision
from supplementary_elements.extra_functions import crop_image
from supplementary_elements.bubble_analysis_functions import create_detector, measure_bubbles, measure_bubble_area
from supplementary_elements.clump_detection_functions import detect_foam_clumps, detect_liquid_clumps
from supplementary_elements.foam_height_functions import measure_foam
from roboccino_interfaces.msg import StirringParams

from sensor_msgs.msg import Image 
from std_msgs.msg import Int8
from rclpy.node import Node 
import rclpy 

from cv_bridge import CvBridge
import cv2

import matplotlib.pyplot as plt
import signal
import time
import sys



class ImageAnalyzer(Node):
    def __init__(self):
        super().__init__('image_analyzer')
      
        self.subscription_bubble_image = self.create_subscription(Image, 'bubble_image', 
                                self.bubble_image_callback, 10)
        self.subscription_side_image = self.create_subscription(Image, 'side_image', 
                                self.side_image_callback, 10)
        self.publisher_mixing_params = self.create_publisher(StirringParams, 'stirring_params', 10)
        self.publisher_decision = self.create_publisher(Int8, 'decision', 10)
        
        self.subscription_stirrer_params = self.create_subscription(StirringParams, 
                            'stirrer_motor_command', self.stirrer_params_callback, 10)
        
        self.subscription_error = self.create_subscription(Int8, 'error_signal',
                                                self.error_callback, 10)
        self.publisher_error = self.create_publisher(Int8, 'error_signal', 10)

        self.declare_params()
        self.br = CvBridge()
        self.bubble_camera_ready = False
        self.side_camera_ready = False
        self.bubble_image = []
        self.side_image = []

        self.liquid_clumps = False
        self.foam_clumps = False
        self.bubble_area = 0

        self.time = 0
        self.speed = 0

        signal.signal(signal.SIGINT, self.signal_handler)
        self.read_params()
        self.create_bubble_detectors()
        

    def bubble_image_callback(self, data):
        self.bubble_image = self.br.imgmsg_to_cv2(data)
        self.bubble_camera_ready = True
        cv2.imwrite(f'images/bubble_{self.time}_{self.speed}.png', self.bubble_image)
        side_img_gray = cv2.cvtColor(self.bubble_image, cv2.COLOR_BGR2GRAY)
        clump_detection_fig, clump_matrix, foam_clumps = detect_foam_clumps(side_img_gray, self.bubble_mask_r, 
                                            self.foam_clumps_window_x, self.foam_clumps_window_y, 
                                            self.foam_clumps_threshold)
        bubble_analysis_fig, [masked_merged_img, nr_merged, mean_merged] = measure_bubbles(
                                self.bubble_image, self.detector_phase1, self.detector_phase2, 
                                self.bubble_blur, self.bubble_clustering_k, self.bubble_mask_r, 
                                self.bubble_nr_last_bubbles, self.bubble_r_tolerance, 
                                self.bubble_coord_tolerance)
        area_analysis_fig, percentage = measure_bubble_area(self.bubble_image, self.detector_phase1, 
                                        self.detector_phase2, self.bubble_blur, self.bubble_clustering_k,
                                        self.bubble_mask_r, 40)
        self.bubble_area = percentage
        self.foam_clumps = foam_clumps

        bubble_analysis_fig.savefig(f'images/bubble_analysis_{self.time}_{self.speed}.png')
        clump_detection_fig.savefig(f'images/foam_clumps_{self.time}_{self.speed}.png')
        area_analysis_fig.savefig(f'images/area_analysis_{self.time}_{self.speed}.png')
        if(self.bubble_camera_ready and self.side_camera_ready):
            self.side_camera_ready = False
            self.bubble_camera_ready = False
            self.decision_procedure()
    
    
    def side_image_callback(self, data):
        self.side_image = self.br.imgmsg_to_cv2(data)
        self.side_camera_ready = True
        cv2.imwrite(f"images/side_{self.time}_{self.speed}.png", self.side_image)
        h, w, _ = self.side_image.shape
        foam_img_height = (h * self.side_foam_ratio) // self.side_division
        foam = crop_image(self.side_image, 0, 0, w, foam_img_height)
        liquid = crop_image(self.side_image, 0, foam_img_height, w, h - foam_img_height)
        liquid = cv2.cvtColor(liquid, cv2.COLOR_BGR2GRAY)
        
        foam_fig, height_img, median, mode = measure_foam(foam, self.foam_height_kernel_size, 
                                            self.foam_height_param2, self.foam_height_param2)
        liquid_clumps_fig, liquid_clump_matrix, liquid_clumps = detect_liquid_clumps(liquid, 
                                            self.liquid_clumps_window_x, self.liquid_clumps_window_y, 
                                            self.liquid_clumps_threshold)
        
        liquid_clumps_fig.savefig(f'images/liquid_clumps_{self.time}_{self.speed}.png')
        foam_fig.savefig(f'images/foam_{self.time}_{self.speed}.png')
        
        self.liquid_clumps = liquid_clumps

        if(self.bubble_camera_ready and self.side_camera_ready):
            self.side_camera_ready = False
            self.bubble_camera_ready = False
            self.decision_procedure()


    def decision_procedure(self):
        msg = StirringParams()
        msg_decision = Int8()
        
        time.sleep(1)
        if(self.liquid_clumps == True):
            msg_decision.data = Decision.LIQUID_CLUMPS.value
            msg.time = 5
            msg.speed = 80
            self.publisher_mixing_params.publish(msg)
        elif(self.foam_clumps == True):
            msg_decision.data = Decision.FOAM_CLUMPS.value
            msg.time = 5
            msg.speed = 80
            self.publisher_mixing_params.publish(msg)
        elif(self.bubble_area <= self.good_coffee_threshold):
            msg_decision.data = Decision.READY.value
        else:
            msg_decision.data = Decision.BIG_BUBBLES.value
            msg.time = 5
            msg.speed = 80
            self.publisher_mixing_params.publish(msg)
        
        self.send_error_signal(ErrorHandling.IMAGE_ANALYZER_ERROR)
        #self.publisher_decision.publish(msg_decision)


    def stirrer_params_callback(self, msg):
        self.time = msg.time 
        self.speed = msg.speed


    def send_error_signal(self, msg):
        to_send = Int8()
        to_send.data = msg.value
        self.publisher_error.publish(to_send)
        self.get_logger().info('%s' % msg)
        self.kill_node()


    def error_callback(self, msg):
        self.kill_node()


    def signal_handler(self, signal, frame): 
        self.kill_node()


    def kill_node(self):
        self.destroy_node()
        sys.exit(0)


    def declare_params(self):
        self.declare_parameter('bubble_mask_r', 0)
        self.declare_parameter('bubble_clustering_k', 0)
        self.declare_parameter('bubble_blur', 0)
        self.declare_parameter('bubble_r_tolerance', 0)
        self.declare_parameter('bubble_coord_tolerance', 0)
        self.declare_parameter('bubble_nr_last_bubbles', 0)
        self.declare_parameter('foam_clumps_window_x', 0)
        self.declare_parameter('foam_clumps_window_y', 0)
        self.declare_parameter('foam_clumps_threshold', 0)
        self.declare_parameter('foam_height_kernel_size', 0)
        self.declare_parameter('foam_height_param1', 0)
        self.declare_parameter('foam_height_param2', 0)
        self.declare_parameter('side_division', 0)
        self.declare_parameter('side_foam_ratio', 0)
        self.declare_parameter('liquid_clumps_window_x', 0)
        self.declare_parameter('liquid_clumps_window_y', 0)
        self.declare_parameter('liquid_clumps_threshold', 0)
        self.declare_parameter('good_coffee_threshold', 0)
        for i in range(2):
            nr = i + 1
            self.declare_parameter(f'bubble_detector{nr}_minthreshold', 0)
            self.declare_parameter(f'bubble_detector{nr}_maxthreshold', 0)
            self.declare_parameter(f'bubble_detector{nr}_minconvexity', 0.0)
            self.declare_parameter(f'bubble_detector{nr}_maxconvexity', 0.0)
            self.declare_parameter(f'bubble_detector{nr}_minarea', 0)
            self.declare_parameter(f'bubble_detector{nr}_maxarea', 0)
   

    def get_param_int(self, param_name):
        return self.get_parameter(param_name).get_parameter_value().integer_value


    def get_param_double(self, param_name):
        return self.get_parameter(param_name).get_parameter_value().double_value


    def create_bubble_detectors(self):
        for i in range(2):
            nr = i + 1
            min_thr = self.get_param_int(f'bubble_detector{nr}_minthreshold')
            max_thr = self.get_param_int(f'bubble_detector{nr}_maxthreshold')
            min_conv = self.get_param_double(f'bubble_detector{nr}_minconvexity')
            max_conv = self.get_param_double(f'bubble_detector{nr}_maxconvexity')
            min_area = self.get_param_int(f'bubble_detector{nr}_minarea')
            max_area = self.get_param_int(f'bubble_detector{nr}_maxarea')

            if(nr == 1):
                self.detector_phase1 = create_detector(min_thr, max_thr, min_convexity = min_conv, 
                        max_convexity = max_conv, min_area = min_area, max_area = max_area)
            else:
                self.detector_phase2 = create_detector(min_thr, max_thr, min_convexity = min_conv, 
                        max_convexity = max_conv, min_area = min_area, max_area = max_area)


    def read_params(self):
        self.bubble_mask_r = self.get_param_int('bubble_mask_r')
        self.bubble_clustering_k = self.get_param_int('bubble_clustering_k')
        self.bubble_blur = self.get_param_int('bubble_blur')
        self.bubble_r_tolerance = self.get_param_int('bubble_r_tolerance')
        self.bubble_coord_tolerance = self.get_param_int('bubble_coord_tolerance')
        self.bubble_nr_last_bubbles = self.get_param_int('bubble_nr_last_bubbles')
        self.foam_clumps_window_x = self.get_param_int('foam_clumps_window_x')
        self.foam_clumps_window_y = self.get_param_int('foam_clumps_window_y')
        self.foam_clumps_threshold = self.get_param_int('foam_clumps_threshold')
        self.foam_height_kernel_size = self.get_param_int('foam_height_kernel_size')
        self.foam_height_param1 = self.get_param_int('foam_height_param1')
        self.foam_height_param2 = self.get_param_int('foam_height_param2')
        self.side_division = self.get_param_int('side_division')
        self.side_foam_ratio = self.get_param_int('side_foam_ratio')
        self.liquid_clumps_window_x = self.get_param_int('liquid_clumps_window_x')
        self.liquid_clumps_window_y = self.get_param_int('liquid_clumps_window_y')
        self.liquid_clumps_threshold = self.get_param_int('liquid_clumps_threshold')
        self.good_coffee_threshold = self.get_param_int('good_coffee_threshold')


def main(args=None):
  
    rclpy.init(args=args)
  
    image_analyzer = ImageAnalyzer()
    rclpy.spin(image_analyzer)
  
    image_analyzer.kill_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
