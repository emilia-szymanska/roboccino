import cv2
import sys
import signal
import time

import rclpy
from rclpy.node import Node
from rclpy.exceptions import ParameterNotDeclaredException
from rcl_interfaces.msg import ParameterType
from sensor_msgs.msg import Image
from std_msgs.msg import Int8
import cv2
from cv_bridge import CvBridge 

from supplementary_elements.camera_enum import CameraState
from supplementary_elements.error_enum import ErrorHandling
from supplementary_elements.extra_functions import crop_image

FOCUS  = 28
WIDTH  = 3
HEIGHT = 4
CODEC  = 6

class Camera(Node):

    def __init__(self):
        super().__init__('camera')
        
        self.declare_parameter('device', '/dev/video0')
        self.declare_parameter('focus', 0)
        self.declare_parameter('preprocessing_type', 'none')
        self.declare_parameter('waiting_time', 0)
        self.declare_parameter('skipped_frames', 0)
        self.declare_parameter('crop_x', 0)
        self.declare_parameter('crop_y', 0)
        self.declare_parameter('crop_width', 0)
        self.declare_parameter('crop_height', 0)

        self.subscription_camera_cmd = self.create_subscription(Int8, 'camera_command',
                                                self.command_callback, 10)
        self.publisher_status = self.create_publisher(Int8, 'camera_status', 10)
        self.publisher_processed_img = self.create_publisher(Image, 'image', 10)
        
        self.subscription_error = self.create_subscription(Int8, 'error_signal',
                                                self.error_callback, 10)
        self.publisher_error = self.create_publisher(Int8, 'error_signal', 10)
        
        self.opened = False
        self.br = CvBridge()
        signal.signal(signal.SIGINT, self.signal_handler)


    def init_camera(self):
        camera_device = self.get_parameter('device').get_parameter_value().string_value
        self.preprocessing_type = self.get_parameter('preprocessing_type').get_parameter_value().string_value
        self.x = self.get_parameter('crop_x').get_parameter_value().integer_value
        self.y = self.get_parameter('crop_y').get_parameter_value().integer_value
        self.width = self.get_parameter('crop_width').get_parameter_value().integer_value
        self.height = self.get_parameter('crop_height').get_parameter_value().integer_value
        focus = self.get_parameter('focus').get_parameter_value().integer_value
        self.waiting_time = self.get_parameter('waiting_time').get_parameter_value().integer_value
        self.skipped_frames = self.get_parameter('skipped_frames').get_parameter_value().integer_value
        self.camera = cv2.VideoCapture(camera_device)
        
        self.camera.set(FOCUS, focus)
        self.camera.set(CODEC, cv2.VideoWriter.fourcc('M', 'J', 'P', 'G'))
        self.camera.set(WIDTH, 1920)
        self.camera.set(HEIGHT, 1080)

        msg = Int8()
        self.opened = self.camera.isOpened()

        if self.opened:
            msg.data = CameraState.INITIALIZE.value
        else:
            msg.data = CameraState.UNINITIALIZED.value
            self.send_error_signal(ErrorHandling.CAMERA_ERROR)

        self.publisher_status.publish(msg)


    def command_callback(self, msg):
        cmd = CameraState(msg.data)
        if(cmd == CameraState.INITIALIZE):
            self.init_camera()
            self.get_logger().info('%s' % cmd)
        if(cmd == CameraState.TAKE_PHOTO): 
            if self.opened:
                cnt = 0
                time.sleep(self.waiting_time)
                while(cnt != self.skipped_frames):
                    ret, frame = self.camera.read()
                    cnt += 1
                if ret == True:
                    new_msg = Int8()
                    new_msg.data = cmd.value
                    self.publisher_status.publish(new_msg)
                    self.get_logger().info('%s' % cmd)
                    cv2.imwrite(f'images/{self.preprocessing_type}_img_raw.png', frame)
                    processed = self.preprocessing(frame)
                    converted = self.br.cv2_to_imgmsg(processed, 'bgr8')
                    self.publisher_processed_img.publish(converted)
        

    def preprocessing(self, image):
        preprocessed_img = crop_image(image, self.x, self.y, self.width, self.height)
        if(self.preprocessing_type == 'bubbles'):
            pass
        elif(self.preprocessing_type == 'side'):
            pass

        return preprocessed_img


    def send_error_signal(self, msg):
        to_send = Int8()
        to_send.data = msg.value
        self.publisher_error.publish(to_send)
        self.get_logger().info('%s' % msg)
        self.kill_node()


    def error_callback(self, msg):
        self.kill_node()        
    

    def signal_handler(self, signal, frame):
        self.kill_node() 


    def kill_node(self):
        self.destroy_node()
        sys.exit(0)
        


def main(args=None):
    rclpy.init(args=args)

    camera = Camera()
    rclpy.spin(camera)
    
    camera.kill_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
