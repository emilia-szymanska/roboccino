from supplementary_elements.dispenser_enum import DispenserState
from supplementary_elements.error_enum import ErrorHandling
from supplementary_elements.camera_enum import CameraState
from supplementary_elements.decision_enum import Decision
from supplementary_elements.serial_enum import SerialState
from supplementary_elements.stirrer_enum import StirrerState
from supplementary_elements.robot_enum import RobotState
from supplementary_elements.ramp_enum import RampState

from rclpy.exceptions import ParameterNotDeclaredException
from rcl_interfaces.msg import ParameterType
from std_msgs.msg import String, Int8
from rclpy.node import Node
import rclpy

import signal
import time
import sys


class MainController(Node):

    def __init__(self):
        super().__init__('main_controller')
        
        self.publisher_command_powder_dispenser = self.create_publisher(Int8, 
                            'powder_dispenser_command', 10)
        self.subscription_status_powder_dispenser = self.create_subscription(Int8, 
                            'powder_dispenser_status', self.powder_dispenser_status_callback, 10)
        
        self.publisher_command_water_dispenser = self.create_publisher(Int8, 
                            'water_dispenser_command', 10)
        self.subscription_status_water_dispenser = self.create_subscription(Int8, 
                            'water_dispenser_status', self.water_dispenser_status_callback, 10)

        self.publisher_command_bubble_camera = self.create_publisher(Int8, 
                            'bubble_camera_command', 10)
        self.subscription_status_bubble_camera = self.create_subscription(Int8, 
                            'bubble_camera_status', self.bubble_camera_status_callback, 10)
        
        self.publisher_command_side_camera = self.create_publisher(Int8, 
                            'side_camera_command', 10)
        self.subscription_status_side_camera = self.create_subscription(Int8, 
                            'side_camera_status', self.side_camera_status_callback, 10)
        
        self.publisher_command_stirrer = self.create_publisher(Int8, 
                            'stirrer_command', 10)
        self.subscription_status_stirrer = self.create_subscription(Int8, 
                            'stirrer_status', self.stirrer_status_callback, 10)
        
        self.publisher_command_robot = self.create_publisher(Int8, 
                            'robot_command', 10)
        self.subscription_status_robot = self.create_subscription(Int8, 
                            'robot_status', self.robot_status_callback, 10)
        
        self.subscription_decision = self.create_subscription(Int8, 
                            'decision', self.decision_callback, 10)
        
        self.publisher_command_motor_serial = self.create_publisher(Int8, 
                            'serial_motor_command', 10)
        self.subscription_status_motor_serial = self.create_subscription(Int8, 
                            'serial_motor_status', self.motor_serial_status_callback, 10)
        
        self.publisher_command_servo_serial = self.create_publisher(Int8, 
                            'serial_servo_command', 10)
        self.subscription_status_servo_serial = self.create_subscription(Int8, 
                            'serial_servo_status', self.servo_serial_status_callback, 10)
        
        self.publisher_command_ramp = self.create_publisher(Int8, 
                            'ramp_command', 10)
        self.subscription_status_ramp = self.create_subscription(Int8, 
                            'ramp_status', self.ramp_status_callback, 10)
        
        self.subscription_error = self.create_subscription(Int8, 'error_signal',
                                                self.error_callback, 10)
        self.publisher_error = self.create_publisher(Int8, 'error_signal', 10)


        self.powder_dispenser_status = DispenserState.UNINITIALIZED
        self.water_dispenser_status = DispenserState.UNINITIALIZED
        self.bubble_camera_status = CameraState.UNINITIALIZED
        self.side_camera_status = CameraState.UNINITIALIZED
        self.robot_status = RobotState.UNINITIALIZED
        self.stirrer_status = StirrerState.UNINITIALIZED
        self.motor_serial_status = SerialState.UNINITIALIZED
        self.servo_serial_status = SerialState.UNINITIALIZED
        self.ramp_status = RampState.UNINITIALIZED
        self.decision = Decision.WAITING
        
        signal.signal(signal.SIGINT, self.signal_handler)
        
        time.sleep(3)
        self.initialize_peripherals()


    def initialize_peripherals(self):
        self.send_command_motor_serial(SerialState.INITIALIZE)
        self.send_command_servo_serial(SerialState.INITIALIZE)
        time.sleep(2)
        self.send_command_water_dispenser(DispenserState.INITIALIZE)
        self.send_command_powder_dispenser(DispenserState.INITIALIZE)
        self.send_command_bubble_camera(CameraState.INITIALIZE)
        self.send_command_side_camera(CameraState.INITIALIZE)
        self.send_command_stirrer(StirrerState.INITIALIZE)
        self.send_command_ramp(RampState.INITIALIZE)
        #self.send_command_robot(RobotState.INITIALIZE)
        self.send_command_robot(RobotState.POSE_FOR_MIXING)
        #self.send_command_robot(RobotState.MOVE_TO_PHOTO)
        #self.send_command_robot(RobotState.MOVE_TO_WATER_DISPENSER)
        #self.send_command_robot(RobotState.MOVE_ABOVE_THE_CUP)


    def water_dispenser_status_callback(self, msg):
        info = DispenserState(msg.data)
        self.water_dispenser_status = info

        if(info == DispenserState.POUR):
            if(self.stirrer_status == StirrerState.STIR and
                self.water_dispenser_status == DispenserState.POUR):
                self.send_command_robot(RobotState.MOVE_ABOVE_THE_CUP)
        elif(info == DispenserState.INITIALIZE):
            pass

    
    def powder_dispenser_status_callback(self, msg):
        info = DispenserState(msg.data)
        self.powder_dispenser_status = info

        if(info == DispenserState.INITIALIZE):
            pass
        elif(info == DispenserState.POUR):
            self.send_command_robot(RobotState.MOVE_TO_WATER_DISPENSER)

    
    def bubble_camera_status_callback(self, msg):
        self.bubble_camera_status = CameraState(msg.data)
        if(self.bubble_camera_status == CameraState.TAKE_PHOTO):
        #     and self.side_camera_status == CameraState.TAKE_PHOTO): 
            self.send_command_robot(RobotState.FROM_PHOTO_TO_WAITING)
            self.bubble_camera_status = CameraState.INITIALIZE
        #    self.side_camera_status = CameraState.INITIALIZE


    def side_camera_status_callback(self, msg):
        self.side_camera_status = CameraState(msg.data)
        if(self.side_camera_status == CameraState.TAKE_PHOTO):
            self.send_command_robot(RobotState.MOVE_TO_PHOTO)
            self.side_camera_status = CameraState.INITIALIZE
        '''
        if(self.bubble_camera_status == CameraState.TAKE_PHOTO and
               self.side_camera_status == CameraState.TAKE_PHOTO): 
            self.send_command_robot(RobotState.FROM_PHOTO_TO_WAITING)
            self.bubble_camera_status = CameraState.INITIALIZE
            self.side_camera_status = CameraState.INITIALIZE
        '''

    def stirrer_status_callback(self, msg):
        self.stirrer_status = StirrerState(msg.data)
        if(self.stirrer_status == StirrerState.STIR and
            self.water_dispenser_status == DispenserState.POUR):
            self.send_command_robot(RobotState.MOVE_ABOVE_THE_CUP)


    def robot_status_callback(self, msg):
        self.robot_status = RobotState(msg.data)
        if(self.robot_status == RobotState.INITIALIZE):
            self.send_command_robot(RobotState.INITIAL_POSE)
        
        elif(self.robot_status == RobotState.INITIAL_POSE):
            self.send_command_robot(RobotState.GRAB_CUP_FROM_INITIAL)
        
        elif(self.robot_status == RobotState.GRAB_CUP_FROM_INITIAL):
            self.send_command_robot(RobotState.MOVE_TO_POWDER_DISPENSER)
        
        elif(self.robot_status == RobotState.MOVE_TO_POWDER_DISPENSER):
            self.send_command_powder_dispenser(DispenserState.POUR)
        
        elif(self.robot_status == RobotState.MOVE_TO_WATER_DISPENSER):
            self.send_command_ramp(RampState.SET_MAX)
            self.send_command_robot(RobotState.POSE_FOR_MIXING)

        elif(self.robot_status == RobotState.POSE_FOR_MIXING):
            self.send_command_ramp(RampState.SET_DESIRED)
            time.sleep(1)
            self.send_command_stirrer(StirrerState.STIR)
            self.send_command_water_dispenser(DispenserState.POUR)
        
        elif(self.robot_status == RobotState.MOVE_ABOVE_THE_CUP):
            #self.send_command_robot(RobotState.MOVE_TO_PHOTO)
            if(self.side_camera_status != CameraState.UNINITIALIZED):
                self.send_command_ramp(RampState.SET_MIN)
                self.send_command_side_camera(CameraState.TAKE_PHOTO)
        
        elif(self.robot_status == RobotState.MOVE_TO_PHOTO):
            if(self.bubble_camera_status != CameraState.UNINITIALIZED):
            #    and self.side_camera_status != CameraState.UNINITIALIZED):
                self.send_command_bubble_camera(CameraState.TAKE_PHOTO)
            #    self.send_command_side_camera(CameraState.TAKE_PHOTO)
                
        elif(self.robot_status == RobotState.FROM_PHOTO_TO_WAITING):
            pass

        elif(self.robot_status == RobotState.FROM_WAITING_TO_MIXING or 
             self.robot_status == RobotState.FROM_WAITING_TO_FOAM_CLUMPS):
            self.send_command_stirrer(StirrerState.STIR)

        elif(self.robot_status == RobotState.GRAB_CUP_FROM_MIXING):
            self.send_command_robot(RobotState.FINAL_POSE)
        

        if(self.robot_status == RobotState.FINAL_POSE):
            self.send_error_signal(ErrorHandling.MAIN_CONTROLLER_ERROR)
            #self.send_command_robot(RobotState.INITIAL_POSE)
            


    def decision_callback(self, msg):
        self.decision = Decision(msg.data)
        self.get_logger().info('-----------------------------')
        self.get_logger().info(f'DECISION: %s' % self.decision)
        self.get_logger().info('-----------------------------')

        if(self.decision == Decision.BIG_BUBBLES or self.decision == Decision.LIQUID_CLUMPS):
            self.send_command_robot(RobotState.FROM_WAITING_TO_MIXING)
        elif(self.decision == Decision.READY):
            self.send_command_ramp(RampState.SET_MAX)
            self.send_command_robot(RobotState.GRAB_CUP_FROM_MIXING)
        elif(self.decision == Decision.FOAM_CLUMPS):
            self.send_command_robot(RobotState.FROM_WAITING_TO_FOAM_CLUMPS)
    

    def motor_serial_status_callback(self, msg):
        self.motor_serial_status = SerialState(msg.data)
        if(self.motor_serial_status == SerialState.INITIALIZE):
            pass 

    
    def servo_serial_status_callback(self, msg):
        self.servo_serial_status = SerialState(msg.data)
        if(self.servo_serial_status == SerialState.INITIALIZE):
            pass 
    
    def ramp_status_callback(self, msg):
        self.ramp_status = RampState(msg.data)
        if(self.ramp_status == RampState.INITIALIZE):
            pass
            #self.send_command_ramp(RampState.SET_MAX) 


    def error_callback(self, msg):
        self.kill_node()


    def send_command(self, msg, node):
        to_send = Int8()
        to_send.data = msg.value

        if(node == 'water_dispenser'):
            self.publisher_command_water_dispenser.publish(to_send)
        elif(node == 'powder_dispenser'):
            self.publisher_command_powder_dispenser.publish(to_send)
        elif(node == 'bubble_camera'):
            self.publisher_command_bubble_camera.publish(to_send)
        elif(node == 'side_camera'):
            self.publisher_command_side_camera.publish(to_send)
        elif(node == 'stirrer'):
            self.publisher_command_stirrer.publish(to_send)
        elif(node == 'robot'):
            self.publisher_command_robot.publish(to_send)
        elif(node == 'motor_serial'):
            self.publisher_command_motor_serial.publish(to_send)
        elif(node == 'servo_serial'):
            self.publisher_command_servo_serial.publish(to_send)
        elif(node == 'ramp'):
            self.publisher_command_ramp.publish(to_send)

        self.get_logger().info(f'Command {node}: %s' % msg)


    def send_command_powder_dispenser(self, msg):
        self.send_command(msg, 'powder_dispenser')


    def send_command_water_dispenser(self, msg):
        self.send_command(msg, 'water_dispenser')


    def send_command_bubble_camera(self, msg):
        self.send_command(msg, 'bubble_camera')

    
    def send_command_side_camera(self, msg):
        self.send_command(msg, 'side_camera')
    

    def send_command_stirrer(self, msg):
        self.send_command(msg, 'stirrer')

    
    def send_command_robot(self, msg):
        self.send_command(msg, 'robot')
    

    def send_command_ramp(self, msg):
        self.send_command(msg, 'ramp')

    
    def send_command_motor_serial(self, msg):
        self.send_command(msg, 'motor_serial')


    def send_command_servo_serial(self, msg):
        self.send_command(msg, 'servo_serial')

    def send_error_signal(self, msg):
        to_send = Int8()
        to_send.data = msg.value
        self.publisher_error.publish(to_send)
        self.get_logger().info('%s' % msg)
        self.kill_node()


    def signal_handler(self, signal, frame):
        self.kill_node()


    def kill_node(self):    
        self.destroy_node()
        sys.exit(0)



def main(args=None):
    rclpy.init(args=args)

    controller = MainController()
    rclpy.spin(controller)

    controller.kill_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
