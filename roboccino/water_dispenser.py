from supplementary_elements.dispenser_enum import DispenserState
from supplementary_elements.error_enum import ErrorHandling
from roboccino_interfaces.msg import WaterDispenserParams

from rclpy.exceptions import ParameterNotDeclaredException
from rcl_interfaces.msg import ParameterType
from std_msgs.msg import Int8
from rclpy.node import Node
import rclpy

import signal
import serial
import struct
import sys


class WaterDispenser(Node):

    def __init__(self):
        super().__init__('water_dispenser')
        
        self.declare_parameter('device', '/dev/ttyACM0')
        self.declare_parameter('baud_rate', 9600)
        self.declare_parameter('time', 2.5)
        self.declare_parameter('off', 0)
        self.declare_parameter('on', 180)

        self.subscription_command = self.create_subscription(Int8, 
                            'water_dispenser_command', self.command_callback, 10)
        self.publisher_status = self.create_publisher(Int8, 'water_dispenser_status', 10)
        
        self.publisher_servo_command = self.create_publisher(WaterDispenserParams, 
                                        'dispenser_servo_command', 10)

        self.subscription_error = self.create_subscription(Int8, 'error_signal',
                                                self.error_callback, 10)
        self.publisher_error = self.create_publisher(Int8, 'error_signal', 10)


        signal.signal(signal.SIGINT, self.signal_handler)


    def init_water_dispenser(self):
        self.off = self.get_parameter('off').get_parameter_value().integer_value
        self.on = self.get_parameter('on').get_parameter_value().integer_value
        self.time = int(2 * self.get_parameter('time').get_parameter_value().double_value)


    def command_callback(self, msg):
        cmd = DispenserState(msg.data)
        
        if(cmd == DispenserState.INITIALIZE):
            self.init_water_dispenser()
            self.update_status(cmd)
        elif(cmd == DispenserState.POUR):
            cmd = WaterDispenserParams()
            cmd.on_value = self.on
            cmd.off_value = self.off
            cmd.time = self.time
            self.publisher_servo_command.publish(cmd)


    def update_status(self, msg):
        feedback = Int8()
        feedback.data = msg.value
        self.publisher_status.publish(feedback)
        self.get_logger().info('%s' % msg)
    

    def send_error_signal(self, msg):
        to_send = Int8()
        to_send.data = msg.value
        self.publisher_error.publish(to_send)
        self.get_logger().info('%s' % msg)
        self.kill_node()


    def error_callback(self, msg):
        self.kill_node()


    def signal_handler(self, signal, frame): 
        self.kill_node()


    def kill_node(self):
        self.destroy_node()
        sys.exit(0)


def main(args=None):
    rclpy.init(args=args)

    water_dispenser = WaterDispenser()
    rclpy.spin(water_dispenser)
    
    water_dispenser.kill_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
