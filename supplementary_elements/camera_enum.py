from enum import Enum

class CameraState(Enum):
    UNINITIALIZED = -1
    INITIALIZE = 0
    TAKE_PHOTO = 1

