import cv2
import numpy as np
import statistics
from supplementary_elements.extra_functions import crop_image, clustering, create_circular_mask, get_distance
import matplotlib.pyplot as plt
from skimage.feature import blob_dog, blob_log, blob_doh
from skimage.color import rgb2gray
from skimage.transform import rescale, resize, downscale_local_mean
from math import sqrt


def create_detector(min_threshold, max_threshold, blob_color = -1, min_convexity = -1, 
                    max_convexity = -1, min_area = -1, max_area = -1, min_inertia_ratio = -1, 
                    max_inertia_ratio = -1, min_circularity = -1, max_circularity = -1):
    
    params = cv2.SimpleBlobDetector_Params()

    params.minThreshold = min_threshold
    params.maxThreshold = max_threshold
    
    if(blob_color != -1):
        params.filterByColor = True
        params.blobColor = blob_color

    if(min_convexity != -1 and max_convexity != -1):
        params.filterByConvexity = True
        params.minConvexity = min_convexity
        params.maxConvexity = max_convexity

    if(min_area != -1 and max_area != -1):
        params.filterByArea = True
        params.minArea = min_area
        params.maxArea = max_area

    if(min_inertia_ratio != -1 and max_inertia_ratio != -1):
        params.filterByInertia = True
        params.minInertiaRatio = min_inertia_ratio
        params.maxInertiaRatio = max_inertia_ratio

    if(min_circularity != -1 and max_circularity != -1):
        params.filterByCircularity = True
        params.minCircularity = min_circularity
        params.maxCircularity = max_circularity
    
    detector = cv2.SimpleBlobDetector_create(params)
    return detector



def detect_blob(img, detector, col):
    if(col == 'R'):
        color = (255, 0, 0)
    elif(col == 'G'):
        color = (0, 255, 0)
    elif(col == 'B'):
        color = (0, 0, 255)

    keypoints = detector.detect(img)
    img_keypoints = cv2.drawKeypoints(img, keypoints, np.array([]), 
           color, cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    return img_keypoints, keypoints


def get_bubble_nr_mean(keypoints):
    diameters = []
    for point in keypoints:
        if isinstance(point, cv2.KeyPoint):
            diameters.append(point.size)
        else:
            diameters.append(point[0])

    nr = len(diameters)
    if(nr == 0):
        mean = 0
    else:
        mean = round(statistics.mean(diameters), 2)

    return nr, mean


def merge_detections(all_blobs, big_blobs, no_last_elements, e_r, e_c):
    all_bubbles = []
    big_bubbles = []

    for point in all_blobs:
        all_bubbles.append([point.size, point.pt])
    
    for point in big_blobs:
        big_bubbles.append([point.size, point.pt])

    all_bubbles.sort()
    big_bubbles.sort()
    
    all_bubbles_first = all_bubbles[:len(all_bubbles)-no_last_elements]
    all_bubbles_last = all_bubbles[-no_last_elements:]
    
    to_delete = []

    for i in range(len(big_bubbles)):
        for j in range(len(all_bubbles_last)):
            R = big_bubbles[i][0]
            r = all_bubbles_last[j][0]
            if(R + e_r < r):
                break
            elif(R - e_r <= r):
                X = big_bubbles[i][1][0]
                Y = big_bubbles[i][1][1]
                x = all_bubbles_last[j][1][0]
                y = all_bubbles_last[j][1][1]
                if(X-e_c <= x <= X+e_c and Y-e_c <= y <= Y+e_c):
                    to_delete.append(big_bubbles[i])

    for el in to_delete:
        try:
            big_bubbles.remove(el)
        except:
            print("Element removed")
    
    bubbles = all_bubbles_first + all_bubbles_last + big_bubbles
    return bubbles


def bubble_image_clusterpreprocessing(img, blur):
    preprocessed = cv2.medianBlur(img, blur)
    return preprocessed


def measure_bubble_area(img, detector_phase1, detector_phase2, blur, k, r_val, circle_cut):
    mask_rgb = create_circular_mask(img, r_val, is_rgb = True)
    
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    keypoints_img, keypoints = detect_blob(img, detector_phase1, 'R')
    preprocessed = bubble_image_clusterpreprocessing(img, blur)
    clusters = clustering(preprocessed, k) 
    keypoints_clustered_img, keypoints_clustered = detect_blob(clusters, detector_phase2, 'B')
    
    bubbles = []

    for el in keypoints:
        bubbles.append([el.size, el.pt])

    for el in keypoints_clustered:
        bubbles.append([el.size, el.pt])
    
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    h, w = img_gray.shape
    img_matrix = np.full((h, w), 255, dtype=np.uint8)
    mask_rgb = create_circular_mask(img_gray, r_val, is_rgb = True)
    masked_img = cv2.bitwise_and(img_gray, mask_rgb)

    imgc = cv2.medianBlur(masked_img, 5)
    imgc = cv2.adaptiveThreshold(imgc, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)
    imgthesh = cv2.bitwise_not(imgc)

    blobs_dog = blob_dog(imgthesh, max_sigma=20, threshold=.5)
    blobs_dog[:, 2] = blobs_dog[:, 2] * sqrt(2)
    
    x_c = w // 2
    y_c = h // 2

    for blob in blobs_dog:
        y, x, r = blob
        dist = get_distance(x, y, x_c, y_c)
        if(dist < r_val - circle_cut):
            bubbles.append([2*r, (x, y)])

    for i in range(len(bubbles)):
        d = bubbles[i][0]
        x = int(bubbles[i][1][0])
        y = int(bubbles[i][1][1])
        image_cv = cv2.circle(img_matrix, (x, y), int(d//2), (0, 0, 0), -1)

    for i in range(h):
        for j in range(w):
            dist = get_distance(j, i, x_c, y_c)
            if(dist >= r_val):
                image_cv[i, j] = 127

    white = np.sum(image_cv == 255)
    black = np.sum(image_cv == 0)

    ratio = round(black / white, 2)
    percentage = round((black * 100)/(black+white), 2)

    ax.imshow(image_cv, cmap='gray')
    ax.set_title(f'b={black}, w={white}, r={ratio}, p={percentage}%')

    return fig, percentage



def measure_bubbles(img, detector_phase1, detector_phase2, blur, k, r, last_bubbles_analysis, 
                    radius_tolerance, coordinates_tolerance):
    mask_rgb = create_circular_mask(img, r, is_rgb = True)
    
    fig = plt.figure()

    keypoints_img, keypoints = detect_blob(img, detector_phase1, 'R')
    masked_keypoints_img = cv2.bitwise_and(keypoints_img, mask_rgb)
    nr1, mean1 = get_bubble_nr_mean(keypoints)
    
    ax1 = fig.add_subplot(1, 3, 1)
    ax1.imshow(masked_keypoints_img)
    ax1.set_title(f'#{nr1}, d={mean1}')
    
    preprocessed = bubble_image_clusterpreprocessing(img, blur)
    clusters = clustering(preprocessed, k) 
    keypoints_clustered_img, keypoints_clustered = detect_blob(clusters, detector_phase2, 'B')
    masked_keypoints_clustered_img = cv2.bitwise_and(keypoints_clustered_img, mask_rgb)
    nr2, mean2 = get_bubble_nr_mean(keypoints_clustered)
    
    ax2 = fig.add_subplot(1, 3, 2)
    ax2.imshow(masked_keypoints_clustered_img)
    ax2.set_title(f'#{nr2}, d={mean2}')

    bubbles = merge_detections(keypoints, keypoints_clustered, last_bubbles_analysis, radius_tolerance, 
                                coordinates_tolerance) 
    nr_merged, mean_merged = get_bubble_nr_mean(bubbles)
    for el in bubbles:
        el[1] = (int(el[1][0]), int(el[1][1]))
        el[0] = int(el[0] / 2)
        img = cv2.circle(img, el[1], el[0], (0, 255, 0), 1)
    masked_merged_img = cv2.bitwise_and(img, mask_rgb)
    
    ax3 = fig.add_subplot(1, 3, 3)
    ax3.imshow(masked_merged_img)
    ax3.set_title(f'#{nr_merged}, d={mean_merged}')
    
    return fig, [masked_merged_img, nr_merged, mean_merged]
