from enum import Enum

class RampState(Enum):
    UNINITIALIZED = -1
    INITIALIZE = 0
    SET_MAX = 1
    SET_MIN = 2
    SET_DESIRED = 3
    SET = 4
