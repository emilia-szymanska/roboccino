import cv2
import numpy as np
import statistics
from supplementary_elements.extra_functions import crop_image, clustering
import matplotlib.pyplot as plt

def side_image_preprocessing(img, kernel, clusters = 0):
    processed_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    processed_img = cv2.erode(processed_img, kernel, iterations=1)
    processed_img = cv2.dilate(processed_img, kernel, iterations=1)
    if(clusters != 0):
        processed_img = clustering(processed_img, clusters)
    return processed_img


def foam_height(img, param1, param2):
    edged_img = cv2.Canny(img, param1, param2)

    rows, columns = edged_img.shape
    result = []
    start = 0
    found_edge = 0
    go = True
    j = -1

    for i in range(columns):
        col = edged_img[:, i]
        while(go):
            if(col[j] == 255 and found_edge == 0):
                found_edge = 1
            if(col[j] == 0 and found_edge == 1):
                start = -j
                found_edge = 2
            if(col[j] == 255 and found_edge == 2):
                result.append(-j - start)
                found_edge = 0
                start = 0
                j = 0
                go = False
            if(j == -len(col)):
                result.append(-j - start)
                found_edge = 0
                start = 0
                j = -1
                go = False
            j -= 1
        go = True
    median = int(statistics.median(result))
    mode = int(statistics.mode(result))
    return edged_img, median, mode
    

def measure_foam(img, kernel_size, param1, param2, clusters = 0):
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    processed_img = side_image_preprocessing(img, kernel, clusters)
    height_img, median, mode = foam_height(img, param1, param2)
    
    fig = plt.figure()
    ax1 = fig.add_subplot(1, 2, 1)
    ax1.imshow(processed_img)
    ax1.set_title('Preprocessing')

    ax2 = fig.add_subplot(1, 2, 2)
    ax2.imshow(height_img)
    ax2.set_title(f'median={median}, mode={mode}')

    return fig, height_img, median, mode
